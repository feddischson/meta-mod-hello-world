SUMMARY = "Hello World Driver"
DESCRIPTION = "Hello World driver"
AUTHOR = "Christian Haettich <feddischson@gmail.com>"
SECTION = "Hello_World"

PR = "r0"

inherit module

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
SRC_URI = "file://Makefile file://${BPN}.c"

S = "${WORKDIR}"
